import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.page.html',
  styleUrls: ['./reservation.page.scss'],
})
export class ReservationPage implements OnInit {

  idvoyage;
  idvehicule;
  placesDispo = [];
  places;
  chaises;
  constructor(private serviceData:DataService, private route:ActivatedRoute, private router: Router) { 
    let page = this;
    this.route.queryParams.subscribe(params => {
      if(params && params.voyage){
        this.idvoyage = params.voyage;
        this.idvehicule = params.vehicule;
        this.serviceData.getPlaces(this.idvehicule).subscribe(response =>{
          this.places = response.length;

          this.serviceData.getPlacesDispo(this.idvoyage, this.idvehicule).subscribe(response =>{
            response.forEach(element => {
              let num = element['place'].replace(element['idvehicule'], '');
              this.placesDispo.push(num);
            });
            this.grawPlace(this.places,this.placesDispo);
          });
        });
        
        
      }
    })
  }

  grawPlace(place,placedispo){
      let li=[];
      let nbrcol=4;
      let k=1;
      let nbrligne=(place/nbrcol)+1;
      for(var i=0; i < nbrligne; i++){
        li[i] = {col:[]};
        for(var j=0; j < nbrcol; j++){
          if((i==0 && j < 2) || i > 1 && i < nbrligne-1 && j==2){
            li[i].col[j]=undefined;
          }else{
            let recherche = false;
            for(let temp=0; temp<placedispo.length; temp++){
              if(placedispo[temp]==k){
                recherche=true;
                break;
              }
            }
            if(!recherche){
              li[i].col[j]={"numero":k, "color":"danger"};
              k++;
            }else{
              li[i].col[j]={"numero":k, "color":"success"};
              k++;
            }
          }
        }
      }
      this.chaises=li;
  }

  reserverPlaces(numeroPlaces){
    this.serviceData.reservation(this.idvoyage, numeroPlaces, this.idvehicule).then(res=>{
      alert("Réservation(s) reçue(s)");
    });
  }

  ngOnInit() {
    
  }
}
