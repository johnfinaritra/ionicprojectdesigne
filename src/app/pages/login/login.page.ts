import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { TokenService } from '../../services/token.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private service: DataService, private serviceToken: TokenService, private router: Router) {
      
  }

  ngOnInit() {
  }

  register(form){
    let login = form.value.login;
    let password = form.value.password;
    let result = this.service.connectUser(login, password);
    result.subscribe(data=>{
      this.serviceToken.setToken(data).then(res=>{
        this.router.navigate(['home']);
      });
    }, error=>{
      alert(error['error']);
    });
  }

}
