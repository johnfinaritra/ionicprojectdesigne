import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { TokenService } from 'src/app/services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mesreservations',
  templateUrl: './mesreservations.page.html',
  styleUrls: ['./mesreservations.page.scss'],
})
export class MesreservationsPage implements OnInit {
mesReservations;

  constructor(private dataService:DataService, private tokenService:TokenService, private router:Router) { 
    this.getMesreservations();
  }

  ngOnInit() {
    this.tokenService.getToken().then(tokenObject=>{
      if(tokenObject.token==null || tokenObject.token=="null"){
        this.router.navigate(['/']);
      }
    });
  }

  getMesreservations(){
    return this.dataService.mesReservations().then(response=>{
      response.subscribe(reservation=>{
        this.mesReservations = reservation;
        console.log(reservation);
      }, (error=>{
        this.router.navigate(['login']);
      }));
    });
  }

}
