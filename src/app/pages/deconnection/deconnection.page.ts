import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-deconnection',
  templateUrl: './deconnection.page.html',
  styleUrls: ['./deconnection.page.scss'],
})
export class DeconnectionPage implements OnInit {

  constructor(private tokenService:TokenService, private router: Router) {
    this.tokenService.deconnection().then(res=>{
      this.router.navigate(['home']);
    });
  }

  ngOnInit() {
  }

}
