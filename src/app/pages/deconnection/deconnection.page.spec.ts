import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeconnectionPage } from './deconnection.page';

describe('DeconnectionPage', () => {
  let component: DeconnectionPage;
  let fixture: ComponentFixture<DeconnectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeconnectionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeconnectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
