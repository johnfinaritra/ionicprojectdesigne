import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(public storage:Storage) { }

  getToken(){
    /*return new Promise((resolve, reject)=>{
      this.storage.get("token").then(res=>{
        resolve(res);
      });
    })*/
    return this.storage.get("token").then(res=>{
      return res;
    });
  }

  setToken(tokenValue: Object){
    return this.storage.set('token', tokenValue);
  }

  deconnection(){
    return this.setToken(null);
  }
}
