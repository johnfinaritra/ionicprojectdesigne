import { Injectable } from '@angular/core';
import { baseUrl } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient, private serviceToken: TokenService, private router: Router) { }

  connectUser(email: string, motdepasse: string){
    let user = {
      "id" : "",
      "nom" : "",
      "prenoms" : "",
      "datenaissance" : "",
      "sexe" : "",
      "email" : email,
      "motdepasse" : motdepasse,
      "contact" : ""
    };
    return this.http.post(`${baseUrl}/user/connect`, user, {
      headers:{
        "Accept": 'application/json',
        'Content-Type' : 'application/json'
      }
    });
  }

  inscription(nom: string, prenoms: string, naissance: string, email: string, motdepasse: string, contact: string, sexe:string){
    let user = {
      "id" : "",
      "nom" : nom,
      "prenoms" : prenoms,
      "datenaissance" : naissance,
      "sexe" : sexe,
      "email" : email,
      "motdepasse" : motdepasse,
      "contact" : contact
    };

    return this.http.post(`${baseUrl}/user/inscription`, user, {
      headers:{
        "Accept": 'application/json',
        'Content-Type' : 'application/json'
      }
    });
    /*  .subscribe(data=>{
        console.log(data['_body']);
        let token = "";
        this.serviceToken.setToken(token);
      }, error=>{
        console.log(error);
      });*/
  }

  reservation(idvoyage: string, place:string, idvehicule: string){
    return this.serviceToken.getToken().then(tokenObject=>{
    let token = tokenObject.token;
    let reservation = {
      "id" : "",
      "client" : "",
      "place" : place,
      "idvoyage" : idvoyage,
      "idvehicule" : idvehicule,
      "remarque" : "",
      "etat" : 1
    };
    let headers = {
      "Accept": 'application/json',
      'Content-Type': 'application/json',
      "Authorization": 'Bearer ' + token
    };
    return this.http.post(`${baseUrl}/reservation/ajout`, reservation, {headers : headers})
      .subscribe(data=>{

      }, error=>{
        this.router.navigate(['login']);
      });
    })
  }

  paiement(idReservation: string, montant: string){
    return this.serviceToken.getToken().then(tokenObject=>{
      let token = tokenObject.token;
      let paiement = {
        "id" : "",
        "datePaiement" : "",
        "prix" : montant,
        "reservation" : idReservation,
        "remarque" : "",
        "etat" : 1
      };
      let headers = {
        "Accept": 'application/json',
        'Content-Type': 'application/json',
        "Authorization": "Bearer " + token
      }
      return this.http.post(`${baseUrl}/reservation/paiement`, paiement, {headers : headers})
        .subscribe(data=>{

        }, error=>{
          this.router.navigate(['login']);
        });
    });    
  }

  recherche(depart: string, arrivee:string, datedepart:string, nb:number){
    return this.http.get<any>(`${baseUrl}/recherche/voyage/${depart}/${arrivee}/${datedepart}/${nb}`);
  }

  mesReservations(){
    return this.serviceToken.getToken().then(tokenObject=>{
      let token = tokenObject.token;
      let headers = {
        "Accept": 'application/json',
        'Content-Type': 'application/json',
        "Authorization": 'Bearer ' + token
      }
      return this.http.get<any>(`${baseUrl}/reservation/mesReservation`,{headers : headers});
    });
  }

  detailReservation(idUser: string, idVoyage: string){
    return this.http.get<any>(`${baseUrl}/reservation/detail/${idUser}/${idVoyage}`);
  }

  getExtras(){
    return this.http.get<any>(`${baseUrl}/infos/extras`);
  }

  getExtrasReservation(idReservation: string){
    return this.http.get<any>(`${baseUrl}/reservation/extras/${idReservation}`);
  }

  getPlacesDispo(idVoyage: string, vehicule: string){
    return this.http.get<any>(`${baseUrl}/reservation/dispo/${idVoyage}/${vehicule}`);
  }

  getPlaces(vehicule: string){
    return this.http.get<any>(`${baseUrl}/reservation/placestotal/${vehicule}`);
  }

  getTrajets(){
    return this.http.get<any>(`${baseUrl}/trajet/liste`);
  }

}
