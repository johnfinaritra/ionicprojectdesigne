import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
import { TokenService } from '../services/token.service';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  result : any;
  trajets;

  constructor(private service: DataService, private serviceToken: TokenService, private router:Router) {
    this.service.getTrajets().subscribe(res=>{
      this.trajets = res;
    });
  }

  gotoDetails(idVoyage, idVehicule){
    let navigationExtras : NavigationExtras = {
      queryParams:{
        voyage : idVoyage,
        vehicule: idVehicule
      }
    };
    this.router.navigate(['reservation'], navigationExtras);
  }

  rechercher(lieuDepart, lieuArrive, dateDepart, passagers){
    /*console.log(trajet);
    let lieux = trajet.split('-');
    let lieuDepart = lieux[0];
    let lieuArrive = lieux[1];*/
    this.service.recherche(lieuDepart, lieuArrive, dateDepart, passagers).subscribe(response=>{
      this.result = response;
    });
  }
}
